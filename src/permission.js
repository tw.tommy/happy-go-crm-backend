import router from './router'
import { constantRoutes, asyncRoutes, resetRouter } from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'
import { getAllRoute } from '@/router/action.js'
import { componentsMap } from '@/router/componentsMap'
import { initConnection } from '@/bulletin.js'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login'] // no redirect whitelist

router.beforeEach(async(to, from, next) => {
  // start progress bar
  NProgress.start()

  // set page title
  document.title = getPageTitle(to.meta.title)

  // determine whether the user has logged in
  const hasToken = getToken()
  if (hasToken) {
    if (to.path === '/login') {
      // if is logged in, redirect to the home page
      next({ path: '/' })
      NProgress.done()
    } else {
      const hasGetUserInfo = store.getters.name
      if (hasGetUserInfo) {
        next()
      } else {
        try {
          initConnection()
          // get user info
          await store.dispatch('user/getInfo')
          /* for (var i = 0; i < asyncRoutes.length; i++) {
            const index = constantRoutes.findIndex(
              x => x.path === asyncRoutes[i].path
            )
            if (index !== undefined) {
              constantRoutes.splice(index, 1)
            }
          } */
          constantRoutes.length = 0
          for (var j = 0; j < asyncRoutes.length; j++) {
            constantRoutes.push(asyncRoutes[j])
          }
          resetRouter()
          await getAllRoute()
            .then(res => {
              if (res.status === 'OK') {
                const routerList = []
                const data = JSON.parse(res.data)
                for (var i = 0; i < data.length; i++) {
                  var json = JSON.parse(
                    JSON.stringify(data[i]),
                    (key, value) => {
                      if (key === 'component') {
                        return componentsMap[value]
                      } else {
                        return value
                      }
                    }
                  )
                  routerList.push(json)
                  constantRoutes.push(json)
                  // asyncRoutes.push(json)
                }
                routerList.push({ path: '*', redirect: '/404', hidden: true })
                router.addRoutes(routerList)
                next({ ...to, replace: true })
              } else {
                next()
              }
            })
            .catch(res => {
              next()
            })
        } catch (error) {
          // remove token and go to login page to re-login
          await store.dispatch('user/resetToken')
          Message.error(error || 'Has Error')
          next(`/login`)
          NProgress.done()
        }
      }
    }
  } else {
    /* has no token*/

    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next()
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})

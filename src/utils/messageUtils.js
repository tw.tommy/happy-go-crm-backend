import { Message } from 'element-ui'

export function error(mess) {
  Message({
    message: mess,
    showClose: true,
    type: 'error',
    duration: 3 * 1000
  })
}

export function success(mess) {
  Message({
    message: mess,
    showClose: true,
    type: 'success',
    duration: 3 * 1000
  })
}

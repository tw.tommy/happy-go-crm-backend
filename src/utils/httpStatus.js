/**
 * 共用常數
 * =======================================================
 */

/**
 * Ajax 回應代碼
 */
export const httpStatus = {
  OK: 'OK',
  ERROR: 'ERROR',
  EXCEPTION: 'EXCEPTION'
}

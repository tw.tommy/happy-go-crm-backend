import { Message } from 'element-ui'
import request from '@/utils/request'
import { httpStatus } from './httpStatus'
import store from '@/store'

/**
 * 共用常數
 * =======================================================
 */

export function isAdd() {
  return getAction() === 'add'
}

export function isUpdate() {
  return getAction() === 'update'
}

export function isDelete() {
  return getAction() === 'delete'
}

export function isView() {
  return getAction() === 'view'
}

export function isReadonly() {
  const action = getAction()
  const list = ['delete', 'view']
  return $.inArray(action, list) >= 0
}

/**
 * 取得 action
 */
export function getAction() {
  return sessionStorage['action']
}

/**
 * 設定 action
 * @param {String}} value
 */
export function setAction(value) {
  sessionStorage['action'] = value
}

/**
 * 清除 action
 */
export function clearAction() {
  sessionStorage.removeItem('action')
}

/**
 * 取得 row data
 */
export function getRowData() {
  return sessionStorage['rowData']
}

/**
 * 設定 row data
 * @param {String}} value
 */
export function setRowData(value) {
  sessionStorage['rowData'] = value
}

/**
 * 清除 row data
 */
export function clearRowData() {
  sessionStorage.removeItem('rowData')
}

/**
 * 系統設定及參數
 */
export const setting = {
  action: getAction(),
  // labelPosition: getLabelPosition(),
  labelWidth: '13%'
}

/**
 * 依裝置判斷表單的 Label 是在左邊(left)或是上面(top)
 */
export function getLabelPosition() {
  return store.getters.device === 'mobile' ? 'top' : 'left'
}

/**
 * 查詢頁
 * =======================================================
 */

/**
 * 查詢資料
 * @param {String} url 資料來源
 * @param {Object} condition 查詢條件
 * @param {Number} pageSize 分頁大小
 */
export function doQuery(url, condition, pageSize) {
  // 刪除查詢結果
  sessionStorage.removeItem('total')
  sessionStorage.removeItem('listData')

  // 複製一份查詢條件
  condition = Object.assign({}, condition)
  condition.pageSize = pageSize
  condition.current = 1

  // 保存查詢條件
  sessionStorage['url'] = url
  sessionStorage['condition'] = JSON.stringify(condition)
  sessionStorage['pageSize'] = pageSize
  sessionStorage['current'] = 1

  return getData(url, condition).then(response => {
    // 保存查詢結果
    sessionStorage['listData'] = JSON.stringify(response.data)
    sessionStorage['total'] = response.total
    return response
  })
}

/**
 * 列表頁
 * =======================================================
 */

export function listData() {
  return {
    condition: JSON.parse(sessionStorage['condition']),
    total: parseInt(sessionStorage['total']),
    model: JSON.parse(sessionStorage['listData'])
  }
}

/**
 * 計算分頁的行號
 * @param {*} current 第幾頁
 * @param {*} index 第幾筆
 */
export function countRowNum(current, index) {
  if (!current) current = 1
  const pageSize = sessionStorage['pageSize']
  return pageSize * (current - 1) + index + 1
}

/**
 * 查詢資料 (換頁)
 * @param {Number} current 取得第幾頁資料
 */
export function doChangePage(current) {
  const url = sessionStorage['url']
  const condition = JSON.parse(sessionStorage['condition'])
  condition.current = current

  // 保存查詢條件
  sessionStorage['condition'] = JSON.stringify(condition)
  sessionStorage['current'] = current

  return getData(url, condition).then(response => {
    // 保存查詢結果
    sessionStorage['listData'] = JSON.stringify(response.data)
    sessionStorage['total'] = response.total
    response.condition = condition
    return response
  })
}

/**
 * 改分頁大小
 * @param {Number} pageSize 分頁大小
 */
export function doChangeSize(pageSize) {
  sessionStorage['pageSize'] = pageSize
  const condition = JSON.parse(sessionStorage['condition'])
  condition.pageSize = pageSize
  sessionStorage['condition'] = JSON.stringify(condition)
  return doChangePage(1)
}

/**
 * 查詢資料
 * @param {String} url 資料來源
 * @param {Object} condition 查詢條件
 * @param {Number} current 取得第幾頁資料
 * @param {Number} pageSize 分頁大小
 */
export function getData(url, condition) {
  const promise = request({
    url: url,
    method: 'get',
    params: condition
  }).then(response => {
    const status = response.status

    if (status === httpStatus.ERROR) {
      Message({
        message: 'LINE 203, 快來抓我 (COMMON.JS)',
        // message:
        //   response.message !== null && response.message !== ''
        //     ? response.message
        //     : 'LINE 203, 快來抓我 (COMMON.JS)',
        showClose: true,
        type: 'error',
        duration: 30 * 1000
      })
    } else if (status === httpStatus.OK && response.data.length === 0) {
      Message({
        message: '未查詢到資料',
        showClose: true,
        type: 'success',
        duration: 3 * 1000
      })
    }

    return response
  })

  return promise
}

/**
 * 儲存選取列資料
 * @param {Object} row 選取列資料
 * @param {String} action 執行操作
 */
export function saveRowData(row, action) {
  setRowData(JSON.stringify(row))
  setAction(action)
}

/**
 * 明細頁
 * =======================================================
 */

/**
 * 列表頁選取的資料
 */
export function rowData() {
  const rowData = getRowData()
  let data = {}

  if (!isAdd() && rowData) {
    // 不是新增就從 rowData 裡取出資料
    data = Object.assign(data, JSON.parse(rowData))
  }

  return data
}

/**
 * 全部設為唯讀
 * @param {Boolean} readonly
 */
export function readonlyAll(readonly) {
  const appContainer = $('div.app-container')
  const rows = appContainer.find('div.el-form-item') // 要被唯讀的列

  if (appContainer.length > 0) {
    rows.each(function(index, row) {
      const cellContent = $(row).find('div.el-form-item__content') // 要被唯讀的欄位的上層 div
      readOnly(cellContent, readonly)
    })
  }
}

/**
 * 設為唯讀
 * @param {JQuery} cellContent 要被唯讀的欄位的上層 div (jQuery物件)
 * @param {Boolean} readonly 是否唯讀
 */
export function readOnly(cellContent, readonly) {
  const clas = `.el-select, .el-input, .el-checkbox, .el-radio-group, 
                .el-input-number, .el-upload-container, .el-range-editor, .el-slider`
  const control = cellContent.find(clas)

  if (readonly) {
    // 已經是唯讀就直接跳過
    if (cellContent.find('div.plaintext').length > 0) return

    control.addClass('hidden') // 隱藏原始欄位
    let value = ''

    if (cellContent.find('.el-select, .el-input').length > 0) {
      // 取得 input , datepicker, select 的值
      value = cellContent.find('input').val()

      if (cellContent.find('.el-date-editor--time-select').length > 0) {
        // format 時間
        if (value.length === 4) {
          value = `${value.substring(0, 2)}:${value.substring(2, 4)}`
        } else {
          value = `${value.substring(0, 2)}:${value.substring(
            2,
            4
          )}:${value.substring(4, 6)}`
        }
      }
    } else if (cellContent.find('label.el-radio.is-checked').length > 0) {
      // 取得 radio 的值
      value = cellContent.find('label.el-radio.is-checked').text()
    } else if (cellContent.find('label.el-checkbox.is-checked').length > 0) {
      // 取得 checkbox 的值
      const checkboxs = cellContent.find('label.el-checkbox.is-checked')
      const ary = []
      checkboxs.find('span.el-checkbox__label').each(function(index, checkbox) {
        ary.push($(checkbox).text())
      })

      value = ary.join('、')
    } else if (cellContent.find('div.el-upload-container').length > 0) {
      // 取得 upload 的值
      const filenames = cellContent.find('ul.el-upload-list a')
      const ary = []
      filenames.each(function(index, filename) {
        ary.push($(filename).text())
      })

      value = ary.join('、')
    } else if (cellContent.find('.el-range-input').length > 0) {
      // 取得 Date Range Picker 的值
      const ary = []
      cellContent.find('input').each(function(index, dateInput) {
        const val = $(dateInput).val()
        if (val !== '') {
          ary.push(val)
        }
      })

      value = ary.join(' 到 ')
    } else if (cellContent.find('.el-slider')) {
      value = cellContent.find('div.el-slider').attr('aria-valuetext')
    }

    if (value) {
      // 產生唯讀 div
      const html = $(`<div class='plaintext'>${value}</div>`)
      cellContent.append(html)
    }
  } else {
    // 取消唯讀
    cellContent.find('div.plaintext').remove()
    control.removeClass('hidden')
  }
}

/**
 * 執行新增/修改/刪除
 * @param {String} url
 * @param {Object} data
 */
export function doAction(url, data) {
  const promise = request({
    url: url,
    method: 'post',
    data: data
  }).then(response => {
    const status = response.status

    if (status === httpStatus.ERROR) {
      Message({
        message: response.message,
        showClose: true,
        type: 'error',
        dangerouslyUseHTMLString: true
      })
    } else if (status === httpStatus.OK) {
      Message({
        message: '新增成功',
        showClose: true,
        type: 'success'
      })
    }

    return response
  })

  return promise
}

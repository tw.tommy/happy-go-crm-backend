import request from '@/utils/request'

export function getAllRoute(params) {
  return new Promise((resolve, reject) => {
    request({
      url: '/setting/path/url',
      method: 'get',
      params
    })
      .then(res => {
        resolve(res)
      })
      .catch(function(error) {
        reject(error)
      })
  })
}

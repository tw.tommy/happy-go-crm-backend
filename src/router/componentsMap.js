import Layout from '@/layout'
export const componentsMap = {
  Layout: Layout,
  Menu: () => import('@/layout/components/Menu'),
  SubMenu: () => import('@/layout/components/Router')
  /*,
  admin_func_pathFunc_index: () => import('@/views/admin/func/pathFunc/index'),
  admin_path_setting_index: () => import('@/views/admin/path/setting/index'),
  admin_permission_setting_index: () =>
    import('@/views/admin/permission/setting/index'),
  admin_role_insert_index: () => import('@/views/admin/role/insert/index'),
  admin_role_query_index: () => import('@/views/admin/role/query/index'),
  admin_role_update_index: () => import('@/views/admin/role/update/index'),
  admin_user_mgu_index: () => import('@/views/admin/user/mgu/index'),
  example_email_function_index: () =>
    import('@/views/example/email/function/index'),
  example_notification_function_index: () =>
    import('@/views/example/notification/function/index'),
  example_report_function_index: () =>
    import('@/views/example/report/function/index'),
  mainFunction_adjust_mgt_detail: () => import('@/views/basic/adjust/Detail'),
  mainFunction_adjust_mgt_list: () => import('@/views/basic/adjust/List'),
  mainFunction_adjust_mgt_index: () => import('@/views/basic/adjust/Index'),
  usersetting_resetpassword_function_index: () =>
    import('@/views/usersetting/resetpassword/function/index')*/
}

export function path() {
  const requireComponents = require.context('@/views/', true, /\.vue/)

  requireComponents.keys().forEach(fileName => {
    const reqCom = requireComponents(fileName)

    var regExp = new RegExp('/', 'g')
    var key = fileName
      .replace(regExp, '_')
      .substring(2, fileName.length)
      .split('.')[0]
    componentsMap[key] = reqCom.default
  })
}

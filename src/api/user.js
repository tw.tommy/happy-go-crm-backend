import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/authenticate',
    method: 'post',
    data
  })
}

export function getInfo(params) {
  return request({
    url: '/getUserInfo',
    method: 'get',
    params
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}

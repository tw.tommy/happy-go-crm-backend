import request from '@/utils/request'

export function query(params) {
  return request({
    url: '/setting/role/findAll',
    method: 'get',
    params
  })
}

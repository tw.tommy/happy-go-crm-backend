import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import { path } from '@/router/componentsMap'
path()

import '@/icons' // icon
import '@/permission' // permission control
import '@/directive/permission'
import '@/directive/stringUtils'

// font awesome
import 'vue-awesome/icons/flag'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
Vue.component('v-icon', Icon)

// 系統共用元件
import BackToBall from '@/layout/components/BackToBall'
import BackToTopBall from '@/layout/components/BackToTopBall'
import IndexToolPanel from '@/layout/components/IndexToolPanel'
import DetailToolPanel from '@/layout/components/DetailToolPanel'
Vue.component('BackToBall', BackToBall)
Vue.component('BackToTopBall', BackToTopBall)
Vue.component('IndexToolPanel', IndexToolPanel)
Vue.component('DetailToolPanel', DetailToolPanel)

// scoll down to end action
import elTableInfiniteScroll from 'el-table-infinite-scroll'
Vue.use(elTableInfiniteScroll)

import infiniteScroll from 'vue-infinite-scroll'
Vue.use(infiniteScroll)

// set ElementUI lang to EN
Vue.use(ElementUI, { locale })
Vue.use(require('vue-shortkey'))
Vue.config.productionTip = false
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

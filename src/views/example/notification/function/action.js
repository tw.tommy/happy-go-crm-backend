import request from '@/utils/request'

export function broadcast(data) {
  return request({
    url: '/example/notification/function',
    method: 'post',
    data
  })
}

import download from '@/utils/download'

export function downloadFile(params) {
  return download({
    url: '/example/report/function',
    method: 'get',
    params
  })
}

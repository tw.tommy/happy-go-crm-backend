import request from '@/utils/request'

export function sendMail(data) {
  return request({
    url: '/example/email/function',
    method: 'post',
    data
  })
}

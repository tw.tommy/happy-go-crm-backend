export const indexRules = {
  // custId: [{ required: true, message: '請輸入資料', trigger: 'blur' }]
}

export const detailRules = {
  regionId: [{ required: true, message: '請選擇', trigger: 'change' }],
  custId: [{ required: true, message: '請輸入資料', trigger: 'blur' }],
  personId: [{ required: true, message: '請輸入資料', trigger: 'blur' }]
}

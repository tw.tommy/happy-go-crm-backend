import request from '@/utils/request'

export function update(data) {
  return request({
    url: '/usersetting/resetpassword/function',
    method: 'put',
    data
  })
}

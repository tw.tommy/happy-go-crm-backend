export function rules(password, repassword) {
  return {
    currentPassword: [
      { required: true, message: '請輸入目前密碼', trigger: 'blur' },
      { min: 3, max: 12, message: '密碼長度介於6到12之間' }
    ],
    newPassword: [
      { required: true, message: '請輸入新密碼', trigger: 'blur' },
      { min: 6, max: 12, message: '密碼長度介於6到12之間' }
    ],
    reNewPassword: [
      { required: true, message: '請輸入再次輸入新密碼', trigger: 'blur' },
      { min: 6, max: 12, message: '密碼長度介於6到12之間' }
    ]
  }
}

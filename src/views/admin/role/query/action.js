import request from '@/utils/request'
import download from '@/utils/download'
export function query(params) {
  return request({
    url: '/admin/role/query/index/query',
    method: 'get',
    params
  })
}
export function update(data) {
  return request({
    url: '/admin/role/query/index/update',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: '/admin/role/query/index/delete',
    method: 'delete',
    data
  })
}

export function report(params) {
  return download({
    url: '/admin/role/query/index/report',
    method: 'get',
    params
  })
}

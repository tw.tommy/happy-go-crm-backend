import request from '@/utils/request'

export function add(data) {
  return request({
    url: '/admin/role/insert/index/add',
    method: 'post',
    data
  })
}

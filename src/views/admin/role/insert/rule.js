export function insertFormRules() {
  return {
    role: [{ required: true, message: '請輸入角色代碼', trigger: 'blur' }],
    name: [{ required: true, message: '請輸入角色名稱', trigger: 'blur' }],
    enabled: [{ required: true, message: '請選擇激活狀態', trigger: 'blur' }]
  }
}

import request from '@/utils/request'

export function add(data) {
  return request({
    url: '/setting/path/add',
    method: 'post',
    data
  })
}

export function query(params) {
  return request({
    url: '/setting/path/getAllPath',
    method: 'get',
    params
  })
}

export function getFuncPermission(params) {
  return request({
    url: '/setting/funcPermission/list',
    method: 'get',
    params
  })
}

export function getAllPathAndTitle(params) {
  return request({
    url: '/setting/path/getAllPathAndTitle',
    method: 'get',
    params
  })
}

export function addFuncAndPathRel(data) {
  return request({
    url: '/setting/pathRelPermission/add',
    method: 'post',
    data
  })
}

export function getAllRoles(params) {
  return request({
    url: '/setting/role/findAll',
    method: 'get',
    params
  })
}

export function getOneTier(params) {
  return request({
    url: '/setting/path/getOneTier',
    method: 'get',
    params
  })
}

export function getTress(params) {
  return request({
    url: '/setting/path/getTree',
    method: 'get',
    params
  })
}

export function addSettingPathPermission(data) {
  return request({
    url: '/admin/permission/setting/index/add',
    method: 'post',
    data
  })
}

export function getIdByRole(params) {
  return request({
    url: '/setting/pathPermission/getIdByRole',
    method: 'get',
    params
  })
}

export function test(params) {
  return request({
    url: '/paths/',
    method: 'get',
    params
  })
}

export function addApiPermission(data) {
  return request({
    url: '/paths/add',
    method: 'post',
    data
  })
}

export function getCheckApi(params) {
  return request({
    url: '/paths/getCheckApi',
    method: 'get',
    params
  })
}

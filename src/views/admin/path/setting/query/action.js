import request from '@/utils/request'

export function query(params) {
  return request({
    url: '/setting/path/getAllPath',
    method: 'get',
    params
  })
}

export function getOneTierMenu(params) {
  return request({
    url: '/setting/path/getOneTier',
    method: 'get',
    params
  })
}

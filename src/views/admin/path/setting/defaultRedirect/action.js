import request from '@/utils/request'

export function getMenuByParent(params) {
  return request({
    url: '/setting/path/getMenuByParent',
    method: 'get',
    params
  })
}

export function getOneTierMenu(params) {
  return request({
    url: '/setting/path/getOneTier',
    method: 'get',
    params
  })
}

export function updateRedirect(data) {
  return request({
    url: '/admin/path/setting/index/updateRedirect',
    method: 'post',
    data
  })
}

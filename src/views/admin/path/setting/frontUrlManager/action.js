import request from '@/utils/request'

export function test(params) {
  return request({
    url: '/paths/',
    method: 'get',
    params
  })
}

export function query(params) {
  return request({
    url: '/setting/path/getAllPath',
    method: 'get',
    params
  })
}

export function add(data) {
  return request({
    url: '/admin/path/setting/index/add',
    method: 'post',
    data
  })
}

export function getOneTierMenu(params) {
  return request({
    url: '/setting/path/getOneTier',
    method: 'get',
    params
  })
}

export function getMenuByParent(params) {
  return request({
    url: '/setting/path/getMenuByParent',
    method: 'get',
    params
  })
}

export function getPathByParent(params) {
  return request({
    url: '/setting/path/getPathByParent',
    method: 'get',
    params
  })
}

export function updateSort(data) {
  return request({
    url: '/admin/path/setting/index/updateSort',
    method: 'post',
    data
  })
}

export function updatePath(data) {
  return request({
    url: '/admin/path/setting/index/update',
    method: 'post',
    data
  })
}

export function deletePath(data) {
  return request({
    url: '/admin/path/setting/index/delete',
    method: 'delete',
    data
  })
}

export function insertValid() {
  return {
    path: [{ required: true, message: '請輸入路徑', trigger: 'blur' }],
    title: [{ required: true, message: '請輸入標題', trigger: 'blur' }],
    icon: [{ required: true, message: '請選擇ICON', trigger: 'change' }]
  }
}

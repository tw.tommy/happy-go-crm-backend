import request from '@/utils/request'

export function insert(data) {
  return request({
    url: '/admin/user/mgu/index/add',
    method: 'post',
    data
  })
}

export function query(params) {
  return request({
    url: '/admin/user/mgu/index/list',
    method: 'get',
    params
  })
}

export function findByUserId(params) {
  return request({
    url: '/admin/user/mgu/index/' + params,
    method: 'get'
  })
}

export function setting(data) {
  return request({
    url: '/admin/user/mgu/index',
    method: 'put',
    data
  })
}

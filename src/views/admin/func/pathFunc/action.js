import request from '@/utils/request'

export function getFuncPermission(params) {
  return request({
    url: '/setting/funcPermission/list',
    method: 'get',
    params
  })
}

export function getAllPathAndTitle(params) {
  return request({
    url: '/setting/path/getAllPathAndTitle',
    method: 'get',
    params
  })
}

export function addFuncAndPathRel(data) {
  return request({
    url: '/admin/func/pathFunc/index/add',
    method: 'post',
    data
  })
}

export function query(params) {
  return request({
    url: '/admin/func/pathFunc/index/',
    method: 'get',
    params
  })
}

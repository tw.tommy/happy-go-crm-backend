export function insertFuncPermissionRules() {
  return {
    path: [{ required: true, message: '請輸入姓名', trigger: 'change' }],
    permission: [
      {
        type: 'array',
        required: true,
        message: '請選擇至少一個權限',
        trigger: 'change'
      }
    ]
  }
}

import request from '@/utils/request'

export function bulletin(params) {
  return request({
    url: '/example/notification/function',
    method: 'get',
    params
  })
}

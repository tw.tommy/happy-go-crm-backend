import request from '@/utils/request'

export function getAllRoles(params) {
  return request({
    url: '/setting/role/findAll',
    method: 'get',
    params
  })
}

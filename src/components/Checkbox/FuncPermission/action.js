import request from '@/utils/request'

export function getFuncPermission(params) {
  return request({
    url: '/setting/funcPermission/list',
    method: 'get',
    params
  })
}
